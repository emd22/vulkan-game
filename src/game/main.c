#define GLFW_INCLUDE_VULKAN

#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>

#include <stdio.h>
#include <stdarg.h>

void print_error(const char *msg, ...) {
    va_list args;
    va_start(args, msg);
    vprintf("Error: %s\n", args);
    va_end(args);
}

GLFWwindow *window_init(const int *size, const char *name) {
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    GLFWwindow *window;
    window = glfwCreateWindow(size[0], size[1], name, NULL, NULL);
    if (window == NULL) {
    }
    return window;
}

int main() {
    GLFWwindow *window
    window = window_init({800, 600}, "Test Window");

    while (!glfwWindowShouldClose(window)) {
        glfwPollEvents();
    }

    glfwDestroyWindow(window);
    glfwTerminate();
}